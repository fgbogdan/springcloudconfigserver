DROP TABLE IF EXISTS PROPERTIES1;

create table PROPERTIES1 (
  id serial primary key,
  CREATED_AT timestamp ,
  APPLICATION VARCHAR(255),
  PROFILE VARCHAR(255),
  LABEL VARCHAR(255),
  KEY1 VARCHAR(255),
  VALUE1 VARCHAR(255)
 );
