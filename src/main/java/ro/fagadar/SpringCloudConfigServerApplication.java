package ro.fagadar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@EnableConfigServer
public class SpringCloudConfigServerApplication {
    public static void main(String[] args) {
        System.out.println("Spring Cloud Config Server");
        ConfigurableApplicationContext configurableApplicationContext = SpringApplication.run(SpringCloudConfigServerApplication.class, args);
    }
}