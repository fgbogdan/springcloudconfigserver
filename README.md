# SpringCloudConfigServer

Sample Spring Cloud config server using H2

## Description

Sample how to use Spring Cloud Config with a JDBC connection.
Exposes endpoints for configuration purposes dynamically generated based on table items

## Usage

H2 console: http://127.0.0.1:8080/h2-console
- [ ] user/pass sa/sa

Swagger endpoint: http://localhost:8080/swagger-ui/index.htm
- [ ] sample configuration http://localhost:8080/cloud/app/pro/master

## Caveats

application.yml spring.cloud.config.server.jdbc - customised SQL to retrieve data from table
Table name and columns use 1 as sufix because PROPERTIES, KEY and VALUE are special keywords in H2


## General

Official documentation Spring Cloud Config - https://docs.spring.io/spring-cloud-config/docs/current/reference/html/

server will use JDBC implementation

Spring Cloud Config - embedded server with JDBC
update pom


        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-config-server</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-security</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-jdbc</artifactId>
        </dependency>

Update app config using - @EnableConfigServer

update application properties

    cloud:
        config:
            server:
                prefix: /cloud # avoid disabling swagger
                jdbc:
                    order: 1
                    sql: SELECT KEY, VALUE from PROPERTIES where APPLICATION=? and PROFILE=? and LABEL=? 


prefix - if you do not set the prefix - swagger will be overridden
sql - if the table is not in the default format - use proper field names (in this example the table is in the proper format)

IMPORTANT

if you do not care about spring profile - you could use

spring.profiles.active: jdbc
otherwise you need to add a bean

    @Bean
    public JdbcEnvironmentRepository jdbcEnvironmentRepository(JdbcEnvironmentRepositoryFactory factory,
        JdbcEnvironmentProperties environmentProperties) {
        return factory.build(environmentProperties);
    }

Run … and check endpoints in the format
http://localhost:8080/cloud/<APPLICATION>/<PROFILE>/<LABEL>

Sample
http://localhost:8080/cloud/app/pro/master

Response will be

    {
    "name": "app",
    "profiles": [
        "pro"
        ],
    "label": "master",
    "version": null,
    "state": null,
    "propertySources": [
        {
            "name": "app-pro",
            "source": {
                "configA": "Config A",
                "configB": "Config B"
            }
        }
        ]
    }

by default the label is “master” and it could be omitted from html call. So

http://localhost:8080/cloud/app/pro/master
and
http://localhost:8080/cloud/app/pro
will have the same result
also the default value for LABEL could be defined in app properties
For other settings please check documentation




## License
GPL